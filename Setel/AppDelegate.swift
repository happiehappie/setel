//
//  AppDelegate.swift
//  Setel
//
//  Created by Maja Developer on 12/01/2020.
//  Copyright © 2020 JXLIM. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  let locationManager = CLLocationManager()
  static var isConnectedToAssignedWifi = false
  static var isInAssignedRegion = false
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    locationManager.delegate = self
    locationManager.requestAlwaysAuthorization()
    
    let options: UNAuthorizationOptions = [.badge, .sound, .alert]
    UNUserNotificationCenter.current()
      .requestAuthorization(options: options) { success, error in
        if let error = error {
          print("Error: \(error)")
        }
    }
    return true
  }
  
  func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
  }
  
  func handleEvent(for region: CLRegion!, event: String) {
    // Show an alert if application is active
    if UIApplication.shared.applicationState == .active {
      guard let message = name(from: region.identifier) else { return }
      UIViewController.topViewController()?.showAlert(withTitle: event, message: message)
      //this is to confirm it aligns with both 2.4GhZ and 5Ghz config SSID
      
      
    } else {
      // Otherwise present a local notification
      guard let body = name(from: region.identifier) else { return }
      let notificationContent = UNMutableNotificationContent()
      notificationContent.body = event + body
      notificationContent.sound = UNNotificationSound.default
      notificationContent.badge = UIApplication.shared.applicationIconBadgeNumber + 1 as NSNumber
      let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
      let request = UNNotificationRequest(identifier: "location_change",
                                          content: notificationContent,
                                          trigger: trigger)
      UNUserNotificationCenter.current().add(request) { error in
        if let error = error {
          print("Error: \(error)")
        }
      }
    }
  }
  
  func name(from identifier: String) -> String? {
    let geotifications = Geotification.allGeotifications()
    guard let matched = geotifications.filter({
      $0.identifier == identifier
    }).first else { return nil }
    return matched.name
  }
  
  func wifi(from identifier: String) -> String? {
    let geotifications = Geotification.allGeotifications()
    guard let matched = geotifications.filter({
      $0.identifier == identifier
    }).first else { return nil }
    return matched.wifi
  }
  
  // MARK: UISceneSession Lifecycle
  
  func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
  }
  
  func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
  }
}

extension AppDelegate: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
    AppDelegate.isInAssignedRegion = true
    //this is to factor in 2.4Ghz and 5 Ghz
    AppDelegate.isConnectedToAssignedWifi = (UIDevice.current.WiFiSSID ?? "").lowercased().hasPrefix(wifi(from: region.identifier) ?? "")
    if region is CLCircularRegion {
      handleEvent(for: region, event: "Entering")
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
    AppDelegate.isInAssignedRegion = false
    //this is to factor in 2.4Ghz and 5 Ghz
    AppDelegate.isConnectedToAssignedWifi = (UIDevice.current.WiFiSSID ?? "").lowercased().hasPrefix(wifi(from: region.identifier) ?? "")
    if !AppDelegate.isConnectedToAssignedWifi {
      if region is CLCircularRegion {
        handleEvent(for: region, event: "Leaving")
      }
    }
    
  }
}
