//
//  Utilities.swift
//  Setel
//
//  Created by Maja Developer on 12/01/2020.
//  Copyright © 2020 JXLIM. All rights reserved.
//

import UIKit
import MapKit
import SystemConfiguration.CaptiveNetwork

// MARK: Helper Extensions
extension UIViewController {
  func showAlert(withTitle title: String?, message: String?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
  
  class func topViewController(controller: UIViewController? = UIApplication.shared.windows.first { $0.isKeyWindow }?.rootViewController) -> UIViewController? {
    if let navigationController = controller as? UINavigationController {
      return topViewController(controller: navigationController.visibleViewController)
    }
    if let tabController = controller as? UITabBarController {
      if let selected = tabController.selectedViewController {
        return topViewController(controller: selected)
      }
    }
    if let presented = controller?.presentedViewController {
      return topViewController(controller: presented)
    }
    return controller
  }
}

extension MKMapView {
  func zoomToUserLocation() {
    guard let coordinate = userLocation.location?.coordinate else { return }
    let region = MKCoordinateRegion(center: coordinate, latitudinalMeters: 10000, longitudinalMeters: 10000)
    setRegion(region, animated: true)
  }
}

extension UIDevice {
  @objc var WiFiSSID: String? {
    guard let interfaces = CNCopySupportedInterfaces() as? [String] else { return nil }
    let key = kCNNetworkInfoKeySSID as String
    for interface in interfaces {
      guard let interfaceInfo = CNCopyCurrentNetworkInfo(interface as CFString) as NSDictionary? else { continue }
      return interfaceInfo[key] as? String
    }
    return nil
  }
}
