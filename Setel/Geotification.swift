//
//  Geotification.swift
//  Setel
//
//  Created by Maja Developer on 12/01/2020.
//  Copyright © 2020 JXLIM. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class Geotification: NSObject, Codable, MKAnnotation  {
  enum CodingKeys: String, CodingKey {
    case latitude, longitude, radius, identifier, name, eventType, wifi
  }
  
  var coordinate: CLLocationCoordinate2D
  var radius: CLLocationDistance
  var identifier: String
  var name: String
  var title: String? {
    if name.isEmpty {
      return "No Note"
    }
    return name
  }
  var wifi: String
  
  var subtitle: String? {
    return "Radius: \(radius)m"
  }
  
  init(coordinate: CLLocationCoordinate2D, radius: CLLocationDistance, identifier: String, name: String, wifi: String) {
    self.coordinate = coordinate
    self.radius = radius
    self.identifier = identifier
    self.name = name
    self.wifi = wifi
  }
  
  // MARK: Codable
  required init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    let latitude = try values.decode(Double.self, forKey: .latitude)
    let longitude = try values.decode(Double.self, forKey: .longitude)
    coordinate = CLLocationCoordinate2DMake(latitude, longitude)
    radius = try values.decode(Double.self, forKey: .radius)
    identifier = try values.decode(String.self, forKey: .identifier)
    name = try values.decode(String.self, forKey: .name)
    wifi = try values.decode(String.self, forKey: .wifi)
  }
  
  func encode(to encoder: Encoder) throws {
    var container = encoder.container(keyedBy: CodingKeys.self)
    try container.encode(coordinate.latitude, forKey: .latitude)
    try container.encode(coordinate.longitude, forKey: .longitude)
    try container.encode(radius, forKey: .radius)
    try container.encode(identifier, forKey: .identifier)
    try container.encode(name, forKey: .name)
    try container.encode(wifi, forKey: .wifi)
  }
  
}

extension Geotification {
  public class func allGeotifications() -> [Geotification] {
    guard let savedData = UserDefaults.standard.data(forKey: PreferencesKeys.savedItems) else { return [] }
    let decoder = JSONDecoder()
    if let savedGeotifications = try? decoder.decode(Array.self, from: savedData) as [Geotification] {
      return savedGeotifications
    }
    return []
  }
}
