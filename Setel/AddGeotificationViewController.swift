//
//  AddGeotificationViewController.swift
//  Setel
//
//  Created by Maja Developer on 12/01/2020.
//  Copyright © 2020 JXLIM. All rights reserved.
//

import UIKit
import MapKit

protocol AddGeotificationsViewControllerDelegate {
  func addGeotificationViewController(_ controller: AddGeotificationViewController, didAddCoordinate coordinate: CLLocationCoordinate2D,
                                      radius: Double, identifier: String, name: String, wifi: String)
}

class AddGeotificationViewController: UITableViewController {
  
  @IBOutlet weak var doneButton: UIBarButtonItem!
  @IBOutlet weak var radiusTextField: UITextField!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var wifiTextField: UITextField!
  
  var delegate: AddGeotificationsViewControllerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  @IBAction func textFieldEditingChanged(sender: UITextField) {
    self.doneButton.isEnabled = !(self.radiusTextField.text ?? "").isEmpty && !(self.nameTextField.text ?? "").isEmpty && !(self.wifiTextField.text ??  "").isEmpty
  }
  
  @IBAction func onCancel(_ sender: UIBarButtonItem) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func onDone(_ sender: UIBarButtonItem) {
    let coordinate = mapView.centerCoordinate
    let radius = Double(radiusTextField.text!) ?? 0
    let identifier = NSUUID().uuidString
    let name = nameTextField.text
    
    delegate?.addGeotificationViewController(self, didAddCoordinate: coordinate, radius: radius, identifier: identifier, name: name!, wifi: self.wifiTextField.text!)
  }
  
  @IBAction func onCurrentLocation(_ sender: UIBarButtonItem) {
    self.mapView.zoomToUserLocation()
  }
}
